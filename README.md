StackOverflowUsers
------------------

This project would be more appropriately named StackExchangeUsers as the site is configurable in the Model

It loads the first page of users from the specified site and displays them in a table showing:

- Display name
- Reputation
- Gold, silver and bronze badge counts
- Profile image

Image loading
-------------

Profile images are loaded in the background and cached both in memory and on disk.

- The in memory cache size is configurable by number of images.
- A more thorough implementation would allow conifguration by data size and periodic cleanup of on disk caches.
- The in memory cache is released if the app receives a memory warning.
- For testing purposes, the disk cache can be cleared by uncommenting the call to ImageLoader.clearDiskCache in application:didFinishLaunching

StackExchangeAPI
----------------

The beginning of a simple RESTful API

- Provides protocols and data structure for generalized interactions with the API
- Currently only implements methods for retrieving a collection of resources

Third-party code
----------------

The project uses one piece of third-party code, MD5Digest.swift.

- Used to hash profile image urls to derive caching keys for later retrieval
- Avoids inclusion of CommonCrypto framework
