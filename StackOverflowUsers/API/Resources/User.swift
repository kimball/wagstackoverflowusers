//
//  User.swift
//  StackOverflowUsers
//
//  Created by kball on 6/25/18.
//  Copyright © 2018 Wag. All rights reserved.
//

import Foundation

struct UserBadgeCounts: Codable {
    let bronze: Int
    let silver: Int
    let gold: Int
}

/**
 A StackExchangeAPIResource that represents the User resource on the server
 */
struct User: StackExchangeAPIResource {
    
    static let resourceName: String = "user"
    static let resourceLocation: String = "users"
    
    enum CodingKeys: String, CodingKey {
        case displayName = "display_name"
        case reputation = "reputation"
        case badgeCounts = "badge_counts"
        case profileImageURL = "profile_image"
    }
    
    let displayName: String
    let reputation: Int
    let badgeCounts: UserBadgeCounts
    let profileImageURL: URL?

}
