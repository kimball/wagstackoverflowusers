//
//  StackExchangeAPI.swift
//  StackOverflowUsers
//
//  Created by kball on 6/25/18.
//  Copyright © 2018 Wag. All rights reserved.
//

import Foundation

enum StackExchangeAPIError: Error {
    case invalidURL                         // unable to construct a URL for a resource
    case invalidRequestData                 // unable to build request JSON from the provided resource or parameters
    case unexpectedResponse                 // did not receive a valid HTTP response
    case invalidResponseData(Error)         // did not receive a valid JSON response
    case httpError(Int)                     // HTTP error
}

/**
 Protocol for all resources that will be used with the server's RESTful API
 */
protocol StackExchangeAPIResource: Codable {
    static var resourceName: String { get }         // the conanonical name of the resource
    static var resourceLocation: String { get }     // the location of the resource relative to the API base URL
    
}

/**
 A data structure that represents generic collection responses from the server
 Parsed objects are available in the -items property
 */
struct StackExchangeCollectionResponse<T:StackExchangeAPIResource>: Codable {
    let items: [T]
    let hasMore: Bool
    let quotaMax: Int
    let quotaRemaining: Int
    
    enum CodingKeys: String, CodingKey {
        case items = "items"
        case hasMore = "has_more"
        case quotaMax = "quota_max"
        case quotaRemaining = "quota_remaining"
    }
}

extension StackExchangeAPIError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .invalidURL:
            return NSLocalizedString("Invalid URL for resource.", comment: "")
        case .invalidRequestData:
            return NSLocalizedString("Invalid request data.", comment: "")
        case .unexpectedResponse:
            return NSLocalizedString("Unexpected response from the server", comment: "")
        case .invalidResponseData(let error):
            return NSLocalizedString("Invalid response from the server:", comment: "") + " \(error.localizedDescription)"
        case .httpError(let statusCode):
            return NSLocalizedString("HTTP Error:", comment: "") + "\(statusCode)"
        }
    }
}

/**
 Currently only implements getCollection
 */
class StackExchangeAPI {
    static let sharedInstance = StackExchangeAPI()
    
    let baseURL = URL(string:"https://api.stackexchange.com/2.2/")!
    
    /// Accepts an object conforming to StackExchangeAPIResource and additional GET parameters
    /// After loading calls completion: with the loaded collection or an error if the load failed
    func getCollection<T: StackExchangeAPIResource>(resource: T.Type, parameters: [String: String]?, completion: (([T]?, Error?) -> Void)?) {
        
        // Attempt to construct the url for the resource
        guard
            var url = URL(string: resource.resourceLocation, relativeTo: self.baseURL),
            var components = URLComponents(url: url, resolvingAgainstBaseURL: true)
        
        else { completion?(nil, StackExchangeAPIError.invalidURL); return }
        
        if let parameters = parameters, parameters.count > 0 {
            var queryItems: [URLQueryItem] = []
            
            for (key, value) in parameters {
                queryItems.append(URLQueryItem(name: key, value: value))
            }
            
            components.queryItems = queryItems
            
        }

        if let componentsURL = components.url {
            url = componentsURL
        }
        
        // Construct the request
        var request = URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        // load the collection from the server
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard error == nil else { completion?(nil, error); return }

            guard
                let httpResponse = response as? HTTPURLResponse,
                let data = data
            else { completion?(nil, StackExchangeAPIError.unexpectedResponse); return }
            
            guard 200...299 ~= httpResponse.statusCode else {completion?(nil, StackExchangeAPIError.httpError(httpResponse.statusCode)); return }
                
            let decoder = JSONDecoder()
            
            do {
                let jsonObject = try decoder.decode(StackExchangeCollectionResponse<T>.self, from: data)
                completion?(jsonObject.items, nil)
            }
            catch {
                completion?(nil, StackExchangeAPIError.invalidResponseData(error)); return
            }

        }
        
        task.resume()
    }
    
}
