//
//  Extensions.swift
//  StackOverflowUsers
//
//  Created by kball on 6/25/18.
//  Copyright © 2018 Wag. All rights reserved.
//

import UIKit

/**
 UIAlertViewController convenience constructor creates an alert view with title, message, and single button OK
 */
extension UIAlertController {
    convenience init(title: String, message:String) {
        self.init(title: title, message: message, preferredStyle: .alert)
        self.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
    }
}
