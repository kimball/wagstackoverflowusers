//
//  ImageLoader.swift
//  StackOverflowUsers
//
//  Created by kball on 6/25/18.
//  Copyright © 2018 Wag. All rights reserved.
//

import UIKit

class ImageLoader {
    static let sharedInstance = ImageLoader()
    
    let cacheURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
    
    var memoryCacheQueue: [String] = []                 // when the cache is full, images are removed according to this queue
    var memoryCacheStorage: [String: UIImage] = [:]     // cache storage keyed by url MD5 hash
    let memoryCacheLimit = 30                           // maximum number of images to keep in memory
    
    /// Attempts to load the image for the provided url from memory or disk before loading from network.
    /// Caches any image loaded from the network to memory and disk
    /// Calls completion: returning loaded image or nil if the load failed and the url that was requested
    func imageFor(url: URL, completion: ((UIImage?, URL) -> Void)?) {
        
        // hash the url to create a cache key
        let cacheKey = url.absoluteString.utf8.md5.description
        
        // attempt to load from cache
        if let image = self.cachedImageFor(key: cacheKey) {
            completion?(image, url)
            return
        }
        
        // load from network
        let task = URLSession.shared.downloadTask(with: url) { (fileURL, response, error) in
            guard
                let fileURL = fileURL,
                let data = try? Data(contentsOf: fileURL)
                else { completion?(nil, url); return }
            
            guard let image = UIImage(data: data) else { completion?(nil, url); return }
            
            // cache the loaded image
            self.cache(image: image, key: cacheKey)
            
            completion?(image, url)
            
        }
        
        task.resume()
        
    }
    
    func cache(image: UIImage, key:String) {
        
        // cache in memory
        self.memoryCacheStorage[key] = image
        self.memoryCacheQueue.insert(key, at: 0)
        
        // remove the oldest cached image if the in memory cache is over limit
        if self.memoryCacheQueue.count > self.memoryCacheLimit {
            if let poppedKey = self.memoryCacheQueue.popLast() {            // remove from queue
                self.memoryCacheStorage.removeValue(forKey: poppedKey)      // remove from storage
            }
        }
        
        // cahce the image to disk
        guard let data = UIImagePNGRepresentation(image) else { return }
        
        let fileURL = self.cacheURL.appendingPathComponent(key)

        FileManager.default.createFile(atPath: fileURL.path, contents: data, attributes: nil)

    }
    
    func cachedImageFor(key: String) -> UIImage? {
        
        // attempt to load from memory
        if let image = self.memoryCacheStorage[key] {
            return image
        }
        
        // attemt to load from disk
        let fileURL = self.cacheURL.appendingPathComponent(key)

        let image = UIImage(contentsOfFile: fileURL.path)
        
        return image
    }
    
    // clear all images from memory (i.e. if we're under memory pressure)
    func clearInMemoryCache() {
        self.memoryCacheQueue = []
        self.memoryCacheStorage = [:]
    }
    
    // clear all images from disk cache
    func clearDiskCache() {
        guard let files = try? FileManager.default.contentsOfDirectory(atPath: self.cacheURL.path) else { return }
        
        for filePath in files {
            let _ = try? FileManager.default.removeItem(atPath: self.cacheURL.appendingPathComponent(filePath).path)
        }
    }
}
