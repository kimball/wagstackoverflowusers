//
//  Model.swift
//  StackOverflowUsers
//
//  Created by kball on 6/25/18.
//  Copyright © 2018 Wag. All rights reserved.
//

import UIKit

/**
 A mostly superfluous Model singleton.
 
 As this is a simple single view application this storage could be moved into UsersViewController,
 but the values stored here are likely to be used in any extension to the application.
 */
class Model {
    static let sharedInstance = Model()
    
    /// The site to query
    let site = "stackoverflow"
    
    /// display colors for badges
    let goldColor = UIColor(red: 205.0/255.0, green: 164.0/255.0, blue: 0.0, alpha: 1.0)
    let silverColor = UIColor(red: 140.0/255.0, green: 146.0/255.0, blue: 152.0/255.0, alpha: 1.0)
    let bronzeColor = UIColor(red: 195.0/255.0, green: 139.0/255.0, blue: 95.0/255.0, alpha: 1.0)
    
    var users:[User] = []
}
