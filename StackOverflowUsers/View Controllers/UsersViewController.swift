//
//  UsersViewController.swift
//  StackOverflowUsers
//
//  Created by kball on 6/25/18.
//  Copyright © 2018 Wag. All rights reserved.
//

import UIKit

/**
 A table view that loads and displays the first page of users for the site specified in the Model
 */
class UsersViewController: UITableViewController {
    
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var loadingLabel: UILabel!
    
    override func viewDidLoad() {
        // load the users collection
        StackExchangeAPI.sharedInstance.getCollection(resource: User.self, parameters:["site":Model.sharedInstance.site]) { (collection, error) in
            DispatchQueue.main.async {

                guard let collection = collection else {
                    // if there was an error, show an alert and update the header
                    if let error = error {
                        let alert = UIAlertController(title: "Network Error", message: error.localizedDescription)
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                    self.loadingLabel.text = "Network Error"
                    self.loadingActivityIndicator.stopAnimating()
                    return
                }
                
                Model.sharedInstance.users = collection
            
                // hide the header by default
                self.showHeader(false)

                if collection.count == 0 {
                    // but if we got no results, update the header messaging and keep it visible
                    self.loadingLabel.text = "No Results"
                    self.showHeader(true)
                }
                self.tableView.reloadData()
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Model.sharedInstance.users.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell") as? UserCell else { return UserCell() }
        
        let user = Model.sharedInstance.users[indexPath.row]
        
        cell.usernameLabel.text = user.displayName
        cell.reputationLabel.text = "\(user.reputation)"
        
        // construct the badges attributed string
        let goldBadges = "● \(user.badgeCounts.gold)"
        let goldBadgesRange = NSRange.init(location: 0, length: goldBadges.count)
        let silverBadges = "● \(user.badgeCounts.silver)"
        let silverBadgesRange = NSRange.init(location: goldBadgesRange.upperBound + 1, length: silverBadges.count)
        let bronzeBadges = "● \(user.badgeCounts.bronze)"
        let bronzeBadgesRange = NSRange.init(location: silverBadgesRange.upperBound + 1, length: bronzeBadges.count)
        
        let badgesAttributedString = NSMutableAttributedString(string: "● \(user.badgeCounts.gold) ● \(user.badgeCounts.silver) ● \(user.badgeCounts.bronze)")
        badgesAttributedString.setAttributes([NSAttributedStringKey.foregroundColor : Model.sharedInstance.goldColor], range: goldBadgesRange)
        badgesAttributedString.setAttributes([NSAttributedStringKey.foregroundColor : Model.sharedInstance.silverColor], range: silverBadgesRange)
        badgesAttributedString.setAttributes([NSAttributedStringKey.foregroundColor : Model.sharedInstance.bronzeColor], range: bronzeBadgesRange)

        cell.badgesLabel.attributedText = badgesAttributedString
        cell.imageURL = user.profileImageURL
        
        return cell
    }
    
    /// Shows or hides the table view header
    func showHeader(_ show:Bool) {
        guard let headerView = self.tableView.tableHeaderView else { return }
        
        var frame = headerView.frame
        frame.size.height = show ? 44.0 : 0.0
        headerView.frame = frame
        self.tableView.tableHeaderView = nil
        self.tableView.tableHeaderView = headerView
    }
}
