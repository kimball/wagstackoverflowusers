//
//  UserCell.swift
//  StackOverflowUsers
//
//  Created by kball on 6/25/18.
//  Copyright © 2018 Wag. All rights reserved.
//

import UIKit

/**
 UserCell will invoke ImageLoader -imageForURL whenever its -imageURL property is set and respond to every load completion callback, however
 it will return immediately and do nothing if any callback is not for its currently set imageURL
 
 */
class UserCell: UITableViewCell {
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var reputationLabel: UILabel!
    @IBOutlet weak var badgesLabel: UILabel!
    
    var imageURL:URL? {
        didSet {
            // clear the current image
            self.profileImageView.image = nil
            
            guard let imageURL = self.imageURL else { return }
            
            ImageLoader.sharedInstance.imageFor(url: imageURL) { (image, url) in
                // if the callback URL does not match the current URL, do nothing
                // also do nothing if the load failed
                guard let image = image, imageURL == url else { return }
                    
                DispatchQueue.main.async {
                    self.profileImageView.image = image
                    self.activityIndicator.stopAnimating()
                }
                
            }
        }
    }
    
    override func prepareForReuse() {
        self.profileImageView.image = nil
        self.activityIndicator.startAnimating()
    }
}
